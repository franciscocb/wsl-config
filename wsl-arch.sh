#!/bin/bash

function log() {
    echo -e "> $1"
}

if [[ "$UID" -ne "0" ]];
then
    log "ERROR: You must be root to run this script."
    exit 1
fi

## Gets new user to configure
printf "Non-root username: "
read NEW_USER

log "Initializing keyring..."
(echo "yes" | pacman-key --init > /dev/null) || (log "Cannot initialize Keyring" && exit 1)

log "Populating keyring..."
(echo "yes" | pacman-key --populate > /dev/null) || (log "Cannot populate Keyring" && exit 1)

log "Updating archlinux-keyring..."
(echo "yes" | pacman -Syy archlinux-keyring > /dev/null) || (log "Update keyring" && exit 1)

log "Updating all packages..."
(echo "yes" | pacman -Syu)

# Creates user
if [[ "$(id $NEW_USER 2> /dev/null)" -eq "" ]];
then
    log "Creating user '$NEW_USER'..."
    useradd -mG wheel $NEW_USER
    passwd $NEW_USER
    sed -i 's/# %wheel ALL=\(ALL\) ALL/%wheel ALL=\(ALL\) ALL/g' /etc/sudoers

else
    log "User '$NEW_USER' already exists."
fi


log "Shell configuration..."

printf "Do you want to install Oh-my-zsh? (y/N) "
read install_ohmyzsh

case $install_ohmyzsh in

    Y|y|yes)
        log "Installing Oh-my-zsh shell..."
        echo "yes" | pacman -S git zsh curl
        echo y | sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh | sed 's/exec zsh -l/exit/g')" > /dev/null

        chsh -s /bin/zsh $NEW_USER

        printf "Do you want to PIMP your shell? (Y/n)"
        read pimp

        case $pimp in
            n|N|no) ;;
            *)
                echo "yes" | pacman -S figlet lolcat
                echo 'echo "ArchLinux" | figlet | lolcat' >> ~/.zshrc

                cp -r ~/.zshrc ~/.oh-my-zsh /home/$NEW_USER/
                ;;
        esac
        ;;
    *)
        ;;
esac


echo "\n\n On ArchWSL, change the default user:\n\n\t ./Arch.exe config --default-user $NEW_USER"